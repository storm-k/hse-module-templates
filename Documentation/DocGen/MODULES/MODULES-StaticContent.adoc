== Project Description

DQMH modules containing all the changes and addons we made to DQMH to fit our needs

The Modules and Module Templates in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com) and are built for and based on the Delacor Queued Message Handler (DQMH, www.delacor.com).

=== More Information on DQMH

* http://sine.ni.com/nips/cds/view/p/lang/de/nid/213286
* https://forums.ni.com/t5/Reference-Design-Content/Delacor-Queued-Message-Handler-DQMH/ta-p/3536892
* http://forums.ni.com/t5/Delacor-Toolkits-Discussions/bd-p/7120


=== LabVIEW 2016

The VIs are maintained in LabVIEW 2016.

=== Installation

Or "how to (re)use these templates"...

In order to use these modules as templates in your projects, you can open the included LabVIEW project and then save the existing modules - one by one - as templates. Here's how:

- http://delacor.com/documentation/dqmh-html/AddingaNewDQMHModulefromaCustomT.html[DQMH Online Documentation]


=== Dependencies

These modules depend on

* HSE-Libraries: https://dokuwiki.hampel-soft.com/code/open-source/hse-libraries
* HSE-Logger: https://dokuwiki.hampel-soft.com/code/open-source/hse-logger
* DQMH: http://sine.ni.com/nips/cds/view/p/lang/de/nid/213286



=== Usage

If you want to use the framework helpers (hse-application and hse-configuration), you can find more information about our file and project structure at https://dokuwiki.hampel-soft.com/code/common/project-structure.

=== Contributing

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on
how to contribute.

Please get in touch at (office@hampel-soft.com) for any questions.

=== Credits

* Joerg Hampel
* Manuel Sebald
* Alexander Elbert

=== License

This project is licensed under a modified BSD License - see the LICENSE file for details.